$(document).ready(function () {
    $('#page-one-button').click(function () { 
        $('#page-one').hide();
        $(this).hide();
        $('#page-one-footer').css('min-height', '0');
        $('#second-circle').show();
        $('#page-two').show();
    });

    $('#page-two-button').click(function () { 
        $('#page-two').hide();
        $('#page-three').show();
        $('#third-circle').show();
        $('#page-three-button').show();
        $('#billing-amount-text').hide();
    });

    $('#monthly').click(function() {
        $('#annual .subscription__hover-touched').hide();
        $('#annual').removeClass('subscription__plan-selected');
        $(this).addClass('subscription__plan-selected');
        $('#monthly .subscription__hover-touched').show();
        $('.billing-amount').html('$39');
     });
 
     $('#annual').click(function() {
        $('#monthly .subscription__hover-touched').hide();
        $('#monthly').removeClass('subscription__plan-selected');
        $(this).addClass('subscription__plan-selected');
        $('#annual .subscription__hover-touched').show();
        $('.billing-amount').html('$99');
     });

    $('#add-coupon').click( function () {
        $('.subscription__forms').toggle();
        var couponLabel = $(this).children('label');
        couponLabel.html(couponLabel.html() === '+ Add Coupon' ? "- Add Coupon" : '+ Add Coupon');
    });

    $('#coupon').keyup( function() {
        if (!$(this).val()) {
            $('#coupon-invalid').css('opacity', '1');
        }
        else {
            $('#coupon-invalid').css('opacity', '0');
        }
    });

    $('#title-container').click( function () {
        $('.billings__input-container--hidden').show();
        $('#verification-one').addClass('verification');
    });

    $('#billings__save').click( function(e) {
        e.preventDefault();
        var isValid = true;
        $('.verification').each(function() {
           if(!$(this).val()) {
               isValid = false;
               $(this).siblings('small').show();
           }
       })
        if(isValid) {
            $('#billings__edit').show();
            $('#billings__delete').show();
            var titleValue = $("#verification-one").val() ?  $("#verification-one").val() : "Title 1";
            $('#addresses-menu').append('<button class="dropdown-item">' + titleValue + '</button><div class="dropdown-divider"></div>')
            $(this).hide();
        } 
        else {
             $('.verification').keyup(function() {
                $('.verification').each(function() {
                    if(!$(this).val()) {
                        isValid = false;
                        $(this).siblings('small').show();
                    } 
                    else {
                        $(this).siblings('small').hide();
                    }
                })
            })
        }        
    });
    
});